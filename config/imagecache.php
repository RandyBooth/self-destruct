<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'images',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        storage_path('app/public/images_asset/'),
        storage_path('app/public/images_upload/'),
        storage_path('app/public/images_trash/')
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => array(
        'lists'        => 'App\Filters\Image\Lists',
        'lists-tiny'   => 'App\Filters\Image\ListsTiny',
        'share'        => 'App\Filters\Image\Share',
        'single'       => 'App\Filters\Image\Single',
        'single-tiny'  => 'App\Filters\Image\SingleTiny',
        'user-normal'  => 'App\Filters\Image\UserNormal',
        'user-profile' => 'App\Filters\Image\UserProfile',
        'user-small'   => 'App\Filters\Image\UserSmall',
//        'small'        => 'Intervention\Image\Templates\Small',
//        'medium'       => 'Intervention\Image\Templates\Medium',
//        'large'        => 'Intervention\Image\Templates\Large',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */

    'lifetime' => (60 * 24 * 7), // 60 minutes * 24 hours * 7 days = 1 week (10080 minutes)

);
