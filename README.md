
#### Start NPM Watch
* npm run watch

#### Clear NPM
* rm -rf node_modules
* rm package-lock.json yarn.lock
* npm cache clear --force
* npm install

#### Socket.io - Laravel Echo Server
* cd /Users/randy/Dropbox/web/htdocs/randy/laravel-echo-server/
* laravel-echo-server start

#### Idea
* Cache
  * Users - forever?
  * Users id
  * Users point ?
  * User
  * Providers
* User voted
    * Create db user-user voted
    * Increment user points
    * Get cache users id
        * if match
    
Cache users 30 - forever
Cache array users with points - 1 min or forever
Display users

Vote user
    cache user
    how to compare point?
    
---

:users = [
    [id: 1, name: Randy, points: 20],
    [id: 2, name: Abby, points: 15],
    [id: 3, name: Ethan, points: 10],
];


4:user = [id: 4, name: John, points: 1];
5:user = [id: 5, name: Jane, points: 0];

:users_top = [
    [id: 1, points: 20],
    [id: 2, points: 15],
    [id: 3, points: 10],
    [id: 4, points: 1],
];

:users_top = [
    1 => 20,
    2 => 15,
    3 => 10,
    4 => 1,
];

id: 4, points 11

is 4 in top
    is point 11 more than or equal before to previous
        update
else is point 11 more than or equal before to last
    update
    
is count array not equal
    is last not equal
    
:users_top_forever = [
    1 => 20,
    2 => 15,
    3 => 10,
    4 => 1,
];

:users_top = [
    1 => 30,
    2 => 15,
    3 => 10,
    4 => 5,
];
same

:users_top = [
    1 => 30,
    2 => 15,
    4 => 15,
    3 => 10,
];
destroy users and update it

:users_top = [
    1 => 30,
    5 => 16,
    2 => 15,
    4 => 15,
    3 => 10,
];

---
1 hour - 1 day
10 listing - 10 hours
---
24 * $5 = $120
7 days * $120 = $840
52 weeks * $840 = $43,680
28 days * $120 = $3,360
12 months * $3,360 = $40,320

------

30 minutes - 1 day
10 listing - 5 hours
---
48 * $5 = $240
7 days * $240 = $1,680
52 weeks * $840 = $87,360
28 days * $240 = $6,720
12 months * $6,720 = $80,640

48 * $3 = $144
7 days * $144 = $1,008
52 weeks * $1,008 = $52,416
28 days * $240 = $4,032
12 months * $4,032 = $48,384

------
$5.00 / 100 points = 0.05 (winner)

$10.00 / 200 points = 0.05
$10.00 / 250 points = 0.04 (winner)
$10.00 / 300 points = 0.033333333333333

$15.00 / 300 = 0.05
$15.00 / 400 = 0.0375 (winner)
$15.00 / 500 = 0.03

$25.00 / 700 = 0.035714285714286 (winner)
$25.00 / 800 = 0.03125

$30.00 / 800 = 0.0375
$30.00 / 900 = 0.033333333333333 (winner)