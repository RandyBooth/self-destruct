@extends('layouts.master')
@section('title', $user['users']['0']->name . ' - ' . $user['users']['0']->provider)

@section('open-graph')
    <meta property="og:type" content="profile">
    <meta property="og:title" content="{{ $user['users']['0']->name }}">
    <meta property="og:url" content="{{ url()->current() }}">
    <meta property="og:image" content="{{ route('imagecache', ['template' => 'original', 'filename' => $user['users']['0']->image]) }}">
@endsection

@section('script-bottom')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection

@section('content')
    <router-view></router-view>
@endsection