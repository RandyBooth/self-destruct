@extends('layouts.master')
@section('title', 'Login')

@section('script-bottom')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection

@section('content')

    <div class="wrapper-body columns is-multiline">
        <div class="login column is-12">
            <div class="columns is-multiline is-centered">
                <div class="column is-narrow">
                    <h4 class="has-text-centered">Please Log In</h4>
                </div>
            </div>
            <div class="columns is-multiline is-centered">
                <div class="column is-narrow has-text-centered">
                    <a class="button button-socialmedia button-facebook" href="/login/facebook"><i class="fab fa-facebook-f"></i> Log in with Facebook</a>
                </div>
                <div class="column is-narrow has-text-centered">
                    <a class="button button-socialmedia button-twitter" href="/login/twitter"><i class="fab fa-twitter"></i> Log in with Twitter</a>
                </div>
            </div>
        </div>
    </div>
    <div>
    </div>
@endsection