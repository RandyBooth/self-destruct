@extends('layouts.master')
@section('title', 'Destructions')

@section('script-bottom')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection

@section('content')
    <router-view @auth{{'auth'}}@endauth></router-view>
@endsection