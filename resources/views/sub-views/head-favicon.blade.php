    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/apple-touch-icon.png') }}?v=20181028">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}?v=20181028">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}?v=20181028">
    <link rel="manifest" href="{{ asset('/site.webmanifest') }}?v=20181028">
    <link rel="mask-icon" href="{{ asset('/safari-pinned-tab.svg') }}?v=20181028" color="#ff6b6b">
    <meta name="msapplication-TileColor" content="#ff6b6b">
    <meta name="theme-color" content="#ffffff">