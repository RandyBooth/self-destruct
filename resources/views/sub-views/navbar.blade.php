    <nav class="navbar is-transparent">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item" href="{{ route('home') }}">
                    {{ config('app.name') }}
                </a>
                <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

            <div id="navbarExampleTransparentExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="{{ route('users.destruct') }}">Destructions</a>
                    <a class="navbar-item" href="{{ route('points') }}">Buy Points</a>
                </div>

                <div class="navbar-end">
                    @auth('web')
                        <a class="navbar-item" href="{{ route('users.show', ['provider' => strtolower(auth('web')->user()->provider), 'id' => auth('web')->user()->provider_id]) }}">{{ auth('web')->user()->name }}</a>
                        <a class="navbar-item" href="{{ route('points') }}">{{ auth('web')->user()->point }} {{ str_plural('point', auth('web')->user()->point) }}</a>
                    @endauth

                    <div class="navbar-item">
                        <div class="field is-grouped">
                            @auth('web')
                                <p class="control">
                                    <a class="button" href="{{ route('logout') }}">
                                        <span>Logout</span>
                                    </a>
                                </p>
                            @else
                                <p class="control">
                                    <a class="button is-primary" href="{{ route('login') }}">
                                        <span>Login</span>
                                    </a>
                                </p>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
