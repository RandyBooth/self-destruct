@extends('layouts.master-comingsoon')

@section('script-bottom')
    <script src="{{ mix('js/coming-soon.js') }}"></script>
@endsection

@section('content')
    <div class="wrapper-body columns is-multiline has-text-centered">
        <div class="column is-12">
            <div>Self-destruct start on {{ $date }}</div>
        </div>

        <div class="column is-12">
            <coming-soon diff={{ $diff }} leading-zero=false></coming-soon>
        </div>
    </div>
@endsection