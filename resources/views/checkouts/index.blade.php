@extends('layouts.master')
@section('title', 'Payment')

@section('script-top')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

@section('script-bottom')
    <script src="{{ mix('js/stripe.js') }}"></script>
@endsection

@section('content')
    <stripe></stripe>
@endsection