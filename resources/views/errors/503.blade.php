@extends('layouts.master-errors')
@section('title', 'Be right back.')

@section('content')
    <?php $message = $exception->getMessage(); ?>
    @if(!empty($message))
        {{ $exception->getMessage() }}
    @else
        {{ 'Be right back!' }}
    @endif
@endsection