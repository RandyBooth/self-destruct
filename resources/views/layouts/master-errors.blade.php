<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

@yield('script-top')

    @include('sub-views.head-favicon')

</head>
<body>
    @include('sub-views.navbar')

    <div class="errors">
        <div class="container">
            <div class="section">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @yield('content')
                    </div>

                    <div class="column is-12">
                        <a href="{{ route('home') }}">
                            <video playsinline autoplay muted loop>
                                <source src="{{ asset('videos/boom.webm') }}" type="video/webm">
                                <source src="{{ asset('videos/boom.mp4') }}" type="video/mp4">
                            </video>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@yield('script-bottom')

    @include('sub-views.ga-tracking')
</body>
</html>
