<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@hasSection('title')@yield('title') | @endif{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <!-- Scripts (Top) -->
    @yield('script-top')
</head>
<body>
    <nav class="navbar is-transparent">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item" href="{{ route('home') }}">
                    {{ config('app.name') }}
                </a>
                <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

            <div id="navbarExampleTransparentExample" class="navbar-menu">
                <div class="navbar-start">
                    {{--<a class="navbar-item" href="{{ route('home') }}">Home</a>--}}
                    {{--<a class="navbar-item" href="{{ route('users.destruct') }}">Destructions</a>--}}
                    {{--<a class="navbar-item" href="{{ route('points') }}">Buy Points</a>--}}
                </div>

                <div class="navbar-end">
                    @auth('web')
                        <div class="navbar-item">{{ auth('web')->user()->name }}</div>
{{--                        <a class="navbar-item" href="{{ route('points') }}">{{ auth('web')->user()->point }} {{ str_plural('point', auth('web')->user()->point) }}</a>--}}
                    @endauth

                    <div class="navbar-item">
                        <div class="field is-grouped">
                            @auth('web')
                                <p class="control">
                                    <a class="button" href="{{ route('logout') }}">
                                        <span>Logout</span>
                                    </a>
                                </p>
                            @else
                                <p class="control">
                                    {{--<a class="button is-primary" href="{{ route('login') }}">--}}
                                        {{--<span>Login</span>--}}
                                    {{--</a>--}}
                                </p>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="app">
        <div class="wrapper container">
            <div class="section">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts (Bottom) -->
    @yield('script-bottom')

    @env('production')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126968958-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126968958-1');
    </script>
    @endenv
</body>
</html>
