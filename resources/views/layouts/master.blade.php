<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @hasSection('meta-description')<meta name="description" content="@yield('meta-description')">@endif

    <meta name="csrf-token" content="{{ csrf_token() }}">
@yield('open-graph')

    <title>@hasSection('title')@yield('title') | @endif{{ config('app.name') }}</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

@isset($state)
    <script type="text/javascript">
      window.__INITIAL_STATE__ = "{!! addslashes(json_encode($state)) !!}";
    </script>
@endisset
@yield('script-top')

    @include('sub-views.head-favicon')

</head>
<body>
    @include('sub-views.navbar')

    <div id="app">
        <flash-message></flash-message>

        <div class="wrapper container">
            <div class="section">
                <div class="columns is-multiline">
                    <div class="column is-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        @isset($modelLogin)@includeWhen($modelLogin, 'sub-views.model-login')@endisset
    </div>

    <script src="{{ mix('js/bootstrap.js') }}"></script>
@yield('script-bottom')

    @include('sub-views.ga-tracking')
</body>
</html>
