import VueCountdown from '@xkeshi/vue-countdown';

export default {
    data() {
        return {
            users: [],
            votes: []
        };
    },

    props: {
        auth: {
            type: Boolean,
            default: false
        }
    },

    components: {
        'countdown': VueCountdown
    },

    methods: {
        thumbUp(index, id) {
            let user = this.users[index];

            if (user.id === id) {
                if (user.voted === false) {
                    user.voted = true;

                    axios.post('/api/vote/' + id).then((response) => {
                        if (response.data.success) {
                            user.vote = user.vote + 1;
                            this.flash(response.data.message, 'success');
                        } else {
                            user.voted = false;
                            this.flash(response.data.message, 'error');
                        }
                    }).catch((error) => {
                        user.voted = false;

                        if (error.response.status) {
                            if (error.response.status == 401) {
                                this.$store.commit('toggleModelLogin', true)
                            }
                        }

                        if (error.response.data.message) {
                            this.flash(error.response.data.message, 'error');
                        } else if (error.request) {
                            console.log(error.request);
                        } else {
                            console.log('Error', error.message);
                        }
                    });
                } else {
                    this.flash('You already downed \'' + user.name + '\'', 'error');
                }
            }
        },
        loopFriends(data, index = 0, unlimited = false) {
            let str = '';
            let limit = index ? 3 : 5;

            if (unlimited) {
                limit = data.length;
            }

            for (let [index, user] of data.entries()) {
                if (index < limit) {
                    str += '<a style="margin-right: 10px;" href="/users/'+user.provider_slug+'/'+user.provider_id+'"><img style="width: 30px;" class="list-images" src="/images/user-profile/'+user.image+'" title="'+user.name+'" /></a>';
                }
            }

            return str;
        }
    },

    watch: {
        users: function (newValue, oldValue) {
            if (this.auth) {
                let oldValueLength = oldValue.length;
                let newValueLength = newValue.length;
                let length = (oldValueLength > newValueLength) ? newValueLength: oldValueLength;

                for(let i = 0; i < length; i++) {
                    let old_user = oldValue[i];

                    if (old_user['id'] === newValue[i]['id']) {
                        if (old_user['voted']) {
                            newValue[i]['voted'] = true;
                        }
                    } else {
                        for(let j = 0; j < length; j++) {
                            let user = newValue[j];

                            if (old_user['id'] === user['id']) {
                                if (old_user['voted']) {
                                    user['voted'] = true;
                                }
                            }
                        }
                    }
                }

                axios.post('/api/votes').then((response) => {
                    if (response.data.success) {
                        this.votes = response.data.votes;

                        let votes = this.votes;
                        let users = newValue;

                        for(let i = 0; i < votes.length; i++) {
                            let vote = votes[i];

                            if (i in users) {
                                if (vote['id'] === users[i]['id']) {
                                    if (vote['voted']) {
                                        users[i]['voted'] = true;
                                    }
                                } else {
                                    for(let j = 0; j < users.length; j++) {
                                        let user = users[j];

                                        if (vote['id'] === user['id']) {
                                            if (vote['voted']) {
                                                user['voted'] = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }).catch((error) => {
                    if (error.response.status) {
                        if (error.response.status == 401) {
                            this.$store.commit('toggleModelLogin', true)
                        }
                    }

                    if (error.response.data.message) {
                        this.flash(error.response.data.message, 'error');
                    } else if (error.request) {
                        console.log(error.request);
                    } else {
                        console.log('Error', error.message);
                    }
                });
            }
        }
    }
}