export default {
    methods: {
        renew(index, id) {
            if (this.users[index].id === id) {
                axios.post('/api/renew/' + id).then((response) => {
                    if (response.data.success) {
                        this.users[index].destructed = false;
                        this.flash(response.data.message, 'success');
                    } else {
                        this.flash(response.data.message, 'error');
                    }
                }).catch((error) => {
                    if (error.response.status) {
                        if (error.response.status == 401) {
                            this.$store.commit('toggleModelLogin', true)
                        }
                    }

                    if (error.response.data.message) {
                        this.flash(error.response.data.message, 'error');
                    } else if (error.request) {
                        console.log(error.request);
                    } else {
                        console.log('Error', error.message);
                    }
                });
            }
        }
    }
};