/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import Vuex from 'vuex'
import VueFlashMessage from 'vue-flash-message';
import ModelLogin from './components/ModelLogin.vue';

Vue.component('model-login', ModelLogin);

Vue.use(Vuex);
Vue.use(VueFlashMessage, {
    createShortcuts: false,
    messageOptions: {
        timeout: 10000,
        important: true
    },
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Card from './components/Stripe.vue';

Vue.component('stripe', Card);

const store = new Vuex.Store({
    mutations: {
        toggleModelLogin (state, status) {
            if (typeof status !== 'undefined') {
                state.Model.Login.isActive = !state.Model.Login.isActive;
            } else {
                state.Model.Login.isActive = status;
            }
        }
    },
    state: {
        Model: {
            Login: {
                isActive: false
            }
        }
    }
});

const app = new Vue({
    el: '#app',
    store
});