/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    // host: window.location.hostname + ':6001'
    host: process.env.MIX_ECHO_URL + ':6001'
});

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import Vuex from 'vuex'
import VueRouter from 'vue-router';
import VueFlashMessage from 'vue-flash-message';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueFlashMessage, {
    createShortcuts: false,
    messageOptions: {
        timeout: 10000,
        important: true
    },
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Users from './components/Users.vue';
import User from './components/User.vue';
import UsersDestruct from './components/UsersDestruct.vue';
import ModelLogin from './components/ModelLogin.vue';

Vue.component('model-login', ModelLogin);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Users },
        { path: '/users/:provider/:id', component: User },
        { path: '/destructions', component: UsersDestruct },
    ]
});

const store = new Vuex.Store({
    mutations: {
        toggleModelLogin (state, status) {
            if (typeof status !== 'undefined') {
                state.Model.Login.isActive = !state.Model.Login.isActive;
            } else {
                state.Model.Login.isActive = status;
            }
        }
    },
    state: {
        Model: {
            Login: {
                isActive: false
            }
        }
    }
});

const app = new Vue({
    el: '#app',
    router,
    store
});