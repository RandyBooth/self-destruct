<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('provider', 20);
            $table->string('provider_id');
            $table->string('provider_username')->nullable();
            $table->string('provider_profile_id')->nullable();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('password');
	        $table->unsignedSmallInteger('vote')->default(0);
	        $table->smallInteger('point')->default(0);
	        $table->string('image')->nullable();
            $table->rememberToken();
            $table->timestamp('destructed_at')->nullable();
            $table->timestamp('flagged_at')->nullable();
	        $table->timestamp('image_updated_at')->nullable();
	        $table->timestamp('renewed_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
