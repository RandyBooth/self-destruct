<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUserOldVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement('CREATE TABLE user_old_votes LIKE user_votes;');

	    Schema::table('user_old_votes', function (Blueprint $table) {
		    $table->datetime('old_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_old_votes');
    }
}
