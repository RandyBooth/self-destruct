<?php

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'provider' => $faker->randomElement(['Facebook', 'Twitter']),
        'provider_id' => $faker->numberBetween(100000),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make(str_random(60)),
    ];
});
