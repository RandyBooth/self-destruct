<?php

use App\User;
use App\UserVote;
use Faker\Generator as Faker;

$factory->define(UserVote::class, function (Faker $faker) {
    $user_ids = User::pluck('id')->all();
    $is_new = true;

    do {
        $user_id = $faker->randomElement($user_ids);
        $vote_id = $faker->randomElement($user_ids);

        if ($user_id !== $vote_id) {
            $user_vote = UserVote
                ::where('user_id', $user_id)
                ->where('vote_id', $vote_id)
                ->first();

            if (!$user_vote) {
                $is_new = false;
            }
        }
    } while ($is_new);

    return [
        'user_id' => $user_id,
        'vote_id' => $vote_id,
        'ip_address' => $faker->ipv4
    ];
});
