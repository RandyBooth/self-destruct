<?php

use App\User;
use App\UserVote;
use Illuminate\Database\Seeder;

class UserVotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserVote::class, 2000)->create();

        $users = User::with('votes')->get(['id', 'vote']);

        foreach($users as $user) {
            $user->update(['vote' => $user->votes()->count()]);
        }
    }
}
