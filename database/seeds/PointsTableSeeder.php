<?php

use App\Point;
use Illuminate\Database\Seeder;

class PointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $points = [
		    [
			    'name' => 'Starter',
			    'amount' => 4.99,
			    'points' => 100,
		    ],
		    [
			    'name' => 'Basic',
			    'amount' => 14.99,
			    'points' => 400,
		    ],
		    [
			    'name' => 'Pro',
			    'amount' => 29.99,
			    'points' => 900,
		    ],
		    [
			    'name' => 'Friend',
			    'amount' => 0,
			    'points' => -100,
		    ],
	    ];

	    foreach($points as $point) {
		    $point['slug'] = str_slug($point['name']);
		    Point::create($point);
	    }
    }
}
