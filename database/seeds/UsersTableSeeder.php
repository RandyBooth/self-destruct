<?php

use App\Helpers\HelperImage;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 50)->create();
        $path = public_path('img/happy.jpg');

        foreach($users as $user) {
            HelperImage::upload_user_image($path, $user);
        }
    }
}
