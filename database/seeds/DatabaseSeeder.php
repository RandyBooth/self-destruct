<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(PointsTableSeeder::class);

        if (app()->environment('local')) {
            $this->call(UsersTableSeeder::class);
            $this->call(UserVotesTableSeeder::class);
            cache()->flush();
        }
    }
}
