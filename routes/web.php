<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'UserController@index']);
Route::get('/users/{provider}/{provider_id}', ['as' => 'users.show', 'uses' => 'UserController@show']);
Route::get('/destructions', ['as' => 'users.destruct', 'uses' => 'UserController@destruct']);

Route::get('/out/{provider}/{provider_id}', ['as' => 'users.provider_link', 'uses' => 'UserController@provider_link']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/points', ['as' => 'points', 'uses' => 'CheckoutController@index']);
});

Route::group(['prefix' => 'login'], function() {
    Route::group(['prefix' => '{provider}', 'where' => ['provider' => '[a-z]+']], function() {
        Route::get('/', ['as' => 'social_auth.redirect', 'uses' => 'SocialAuthController@redirect']);
        Route::get('callback', ['as' => 'social_auth.callback', 'uses' => 'SocialAuthController@callback']);
    });
});

Route::group(['middleware' => 'guest'], function() {
    Route::get('/login', ['as' => 'login', 'uses' => 'SocialAuthController@login']);
});

Route::get('/logout', ['as' => 'logout', 'uses' => 'SocialAuthController@logout']);

Route::get('/privacy-policy', function () {
    return 'Coming Soon';
});

Route::get('/terms', function () {
    return 'Coming Soon';
});