<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/', ['as' => 'api.home', 'uses' => 'Api\UserController@index']);
//Route::post('/users', ['as' => 'api.users', 'uses' => 'UserController@api_index']);
Route::post('/users/{provider}/{id}', ['as' => 'api.users.show', 'uses' => 'Api\UserController@show']);
Route::post('/destructions', ['as' => 'api.users.destruct', 'uses' => 'Api\UserController@destruct']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/votes', ['middleware' => 'throttle:50,1', 'as' => 'api.votes', 'uses' => 'Api\UserVoteController@votes']);
    Route::post('/vote/{id}', ['middleware' => 'throttle:30,1', 'as' => 'api.vote', 'uses' => 'Api\UserVoteController@up']);
    Route::post('/renew/{id}', ['as' => 'api.users.renew', 'uses' => 'Api\UserController@renew']);
    Route::post('/charge', 'Api\CheckoutController@charge');
});

