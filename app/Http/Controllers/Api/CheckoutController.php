<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\PointTrait;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    use PointTrait;

    public function __construct()
    {
        $this->middleware(
            ['auth']
        );
    }

    public function charge(Request $request)
    {
        $user = auth()->user();
        $stripe = Stripe::make();
        $custom_id = null;
        $card_default_id = null;
        $new_card = false;

        if (!empty($user->stripe_id)) {
            $custom_id = $user->stripe_id;

            if ($request->filled('stripeToken')) {
                $card = $stripe->cards()->create(
                    $custom_id,
                    $request->input('stripeToken')
                );

                if (!empty($card)) {
                    $card_default_id = $card['id'];
                    $new_card = true;
                }
            } else if ($request->filled('cardID')) {
                $card_id_decode = decrypt($request->input('cardID'));

                if (isset($card_id_decode)) {
                    $card_data = $user->cards->where('id', $card_id_decode)->first();

                    if ($card_data) {
                        $card_default_id = $card_data->stripe_id;
                    }
                }
            }
        } else {
            if ($request->filled('stripeToken')) {
                $token = $request->input('stripeToken');

                $customer = $stripe->customers()->create([
                    'email' => $user->email,
                    'source' => $token
                ]);

                if (isset($customer['id'])) {
                    $custom_id = $customer['id'];

                    $user->stripe_id = $custom_id;

                    if ($user->update()) {
                        $card_default_id = $customer['default_source'];
                        $new_card = true;
                    }
                }
            }
        }

        if ($new_card) {
            $cards = $stripe->cards()->all($custom_id, ['limit' => 20]);

            if (!empty($cards['data'])) {
                $stripe_cards = $cards['data'];

                foreach($stripe_cards as $stripe_card) {
                    $expiry_date = $stripe_card['exp_month'].'/'.$stripe_card['exp_year'];

                    $card = [
                        'stripe_id' => $stripe_card['id'],
                        'brand' => $stripe_card['brand'],
                        'last_four' => $stripe_card['last4'],
                        'expiry_date' => $expiry_date,
                        'default' => $card_default_id === $stripe_card['id'],
                    ];

                    $save_card = $user->cards()->updateOrCreate(
                        [
                            'user_id' => $user->id,
                            'stripe_id' => $stripe_card['id'],
                        ],
                        $card
                    );

                    $save_card->save();
                }
            }
        }

        if (!empty($card_default_id)) {
            if ($request->filled('plan')) {
                $plan = $request->input('plan');

//                $this->clearPlanPoints();
                $points = $this->getPlanPoints('slug');

                if (isset($points[$plan])) {
                    $point = $points[$plan];

                    $charge = $stripe->charges()->create([
                        'customer' => $custom_id,
                        'source' => $card_default_id,
                        'currency' => 'USD',
                        'amount'   => $point->amount,
                        'description' => $plan
                    ]);

                    if ($charge) {
                        if (isset($charge['status'])) {
                            if ($charge['status'] === 'succeeded') {
                                $create_point = $user->points()->create(
                                    [
                                        'point_id' => $point->id,
                                        'points' => $point->points,
                                        'stripe_charge_id' => $charge['id'],
                                    ]
                                );

                                if ($create_point) {
                                    $user->point = $user->points->sum('points');

                                    if ($user->save()) {
                                        return response()->json(['success' => true, 'message' => 'Charge is successful!']);
                                    }

                                    return response()->json(['message' => 'Charge is successful but could not save total points.']);
                                }

                                return response()->json(['message' => 'Charge is successful but could not save points info.']);
                            }

                            return response()->json(['message' => 'Charge is not successful. Please try again.']);
                        }
                    }

                    return response()->json(['message' => 'Could not charge it. Please try again.']);
                }

                return response()->json(['message' => 'Could not find the plan.']);
            }

            return response()->json(['message' => 'Plan field is missing.']);
        }

//        return response()->json(['success' => false, 'message' => 'Stripe ID is missing!']);
    }
}
