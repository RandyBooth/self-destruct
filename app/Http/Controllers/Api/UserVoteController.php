<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserVoteResource;
use App\UserVote;
use App\Traits\EloquentUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserVoteController extends Controller
{
	use EloquentUser;

    public function votes()
    {
        $auth = false;
        $votes = collect([]);

        if (auth('api')->check()) {
            $auth = true;
            $users = $this->getUsers();
            $votes = collect($this->getUserVoted($users));
        }

        return collect(['success' => $auth, 'votes' => UserVoteResource::collection($votes)]);
    }

	public function up(Request $request, $id)
	{
		$user_id = auth()->id();

		$vote_user = $this->findUserDecode($id);
		$vote_name = $vote_user->name;

		if (is_null($vote_user->destructed_at)) {
			$user_vote = UserVote::firstOrNew(
				[
					'user_id' => $user_id,
					'vote_id' => $vote_user->id
				],
				['ip_address' => $request->ip()]
			);

			if (!$user_vote->id) {
				if ($user_vote->save()) {
                    $vote_user->increment('vote');
                    $users = $this->getUsers()->pluck('id', 'id');

                    if (array_has($users, $vote_user->id)) {
                        $this->clearCacheUserVotes($user_id);
                    }

                    $this->clearCacheUserVotes($user_id, $vote_user->id);
                    $refresh = $this->check($vote_user);
                    $this->cacheUserVote($vote_user->encoded_id, $vote_user->vote);

                    return response()->json(['success' => true, 'message' => 'You destroyed \''.$vote_name.'\'', 'refresh' => $refresh]);
				}

				return response()->json(['message' => 'Issue with saving \''.$vote_name.'\''], 400);
			}

			return response()->json(['message' => 'You already destroyed \''.$vote_name.'\'']);
		}

		return response()->json(['message' => '\''.$vote_name.'\' is already destructed.'], 403);
	}
}
