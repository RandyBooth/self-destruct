<?php

namespace App\Http\Controllers\Api;

use App\Traits\EloquentUser;
use App\Traits\PointTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	use EloquentUser;
	use PointTrait;

	public function index()
	{
		$state = $this->apiUsers('/');
		return response()->json($state);
	}

    public function show($provider, $provider_id)
    {
        $state = $this->apiUser($provider, $provider_id, '/users/'.$provider.'/'.$provider_id);
        return response()->json($state);
    }

	public function destruct()
	{
		$state = $this->apiUsersDestruct('/destructions');
		return response()->json($state);
	}

    public function renew($id)
    {
        $user = auth()->user();
        $friend = $this->findUserDecode($id);

        if ($friend) {
            if (!is_null($friend->destructed_at)) {
                $point = $this->getFriendPoint();

                if ($point) {
                    $total = $user->point + $point->points;

                    if ($total >= 0) {
                        $create_point = $user->points()->create(
                            [
                                'point_id' => $point->id,
                                'points' => $point->points,
                            ]
                        );

                        if ($create_point) {
                            $user_points = $user->points;
                            $user->point = $user_points->sum('points');

                            if ($user->save()) {
                                $this->saveFriend($user->id, $friend->id);
                                $friend->UndoDestructAndRenewed();
                                $this->clearCacheUserEncodedId($id);
                                $this->clearCacheUserSocial($friend->provider, $friend->provider_id);
                                $this->refreshDestruct();

                                return response()->json(['success' => true, 'message' => 'Thank you for resurrected \''.$friend->name.'\'.']);
                            }

                            return response()->json(['message' => 'Could not save total points.'], 400);
                        }

                        return response()->json(['message' => 'Could not save point info.'], 400);
                    }

                    return response()->json(['message' => 'You need '.abs($total).' more '.str_plural('point', $user->point).'. Please <a href="'.route('points').'">buy more points</a>.']);
                }

                return response()->json(['message' => 'Could not find friend point.'], 400);
            }

            return response()->json(['message' => '\''.$friend->name.'\' has been resurrected.']);
        }

        return response()->json(['message' => 'Could not find the user.'], 400);
    }
}
