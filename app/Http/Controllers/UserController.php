<?php

namespace App\Http\Controllers;

use App\Traits\EloquentUser;
use App\Traits\PointTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use EloquentUser,
	    PointTrait;

    public function index()
    {
        $auth = auth('web')->check() || auth('api')->check();
        $state = [];
        $modelLogin = true;
	    return view('home', compact('auth', 'state', 'modelLogin'));
    }

	public function show($provider, $provider_id)
	{
        $state = [];
        $user = $this->apiUser($provider, $provider_id, '/users/'.$provider.'/'.$provider_id);
        $modelLogin = true;
        return view('users.show', compact('state', 'user', 'modelLogin'));
	}

	public function destruct()
	{
		$state = [];
        $modelLogin = true;
        return view('users.destruct', compact('state', 'modelLogin'));
	}

    public function provider_link($provider, $provider_id)
    {
        $user = $this->findProvider($provider, $provider_id);
        $url = route('home');

        if (!empty($user->provider) && !empty($user->provider_id)) {
            $provider = strtolower($user->provider);

            switch($provider) {
                case 'facebook':
                    if (!empty($user->provider_profile_id)) {
                        $url = 'https://www.facebook.com/app_scoped_user_id/'.$user->provider_profile_id;
                    }
                    break;
                case 'twitter':
                    $url = 'https://twitter.com/intent/user?user_id='.$user->provider_id;
                    break;
            }
        }

        return redirect($url);
    }

    public function destroy_top_user()
    {
        $this->destroyTopUser();
    }

    public function coming_soon()
    {
	    return view('coming-soon');
    }
}
