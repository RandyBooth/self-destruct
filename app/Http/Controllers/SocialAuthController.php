<?php

namespace App\Http\Controllers;

use App\Helpers\HelperImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $provider = '';
    protected $provider_list = [
        'facebook' => 'Facebook',
        'twitter' => 'Twitter',
        'linkedin' => 'LinkedIn',
        'google' => 'Google'
    ];

    public function __construct()
    {
        $this->middleware(
            [
				'guest',
                'throttle:10,1'
            ],
            [
                'only' => [
                    'redirect'
                ]
            ]
        );
    }

    public function login()
    {
        return view('users.login');
    }

    public function redirect($provider)
    {
        if ($this->validProvider($provider)) {
            if ($provider === 'facebook') {
                return Socialite::driver($provider)
                    ->scopes(['user_link'])
                    ->redirect();
            } else {
                return Socialite::driver($provider)
                    ->redirect();
            }
        }
    }

    public function callback($provider)
    {
        if ($this->validProvider($provider)) {
            try {
                $social_user = ($provider === 'facebook')
                    ? Socialite::driver($provider)
                        ->scopes(['user_link'])
                        ->user()
                    : Socialite::driver($provider)
                        ->user();
            } catch (\Exception $e) {
                return redirect('/login');
            }

            if (!empty($social_user->id)) {
                $user = User::withTrashed()->where('provider', '=', $this->provider)->where('provider_id', '=', $social_user->id)->first();

                if ($user) {
                    $now = now();

                    if ($user->updated_at->diffInDays($now) >= 1) {
                        $updated_timestamp = $user->updated_at->timestamp;
                        $user->provider_username = $social_user->nickname;
                        $user->name = $social_user->name;
                        $user->email = $social_user->email;

                        if (isset($social_user->nickname)) {
                            if (!is_null($social_user->nickname)) {
                                $user->provider_username = $social_user->nickname;
                            }
                        }

                        if (isset($social_user->profileUrl)) {
                            $user->provider_profile_id = $social_user->profileUrl;
                        }

                        if (isset($social_user->profileUrl)) {
                            $pattern = '/(https?:\/\/)?(www\.)?facebook\.com\/(app_scoped_user_id*\/)*([A-Za-z0-9]+)[?]?.*/';
                            $url = $social_user->profileUrl;

                            if (preg_match($pattern, $url, $match)) {
                                $user->provider_profile_id = $match[4];
                            }
                        }

                        $user->save();
                        $updated_timestamp_new = $user->updated_at->timestamp;

                        if ($updated_timestamp === $updated_timestamp_new) {
                            $user->touch();
                        }
                    }

                    $run_image = false;

                    if (!empty($user->image_updated_at)) {
                        if ($user->image_updated_at->diffInDays($now) >= 2) {
                            $run_image = true;
                        }
                    } else {
                        $run_image = true;
                    }

                    if ($run_image) {
                        HelperImage::upload_user_image($social_user->avatar_original, $user);
                    }
                } else {
                    $data = [
                        'provider' => $this->provider,
                        'provider_id' => $social_user->id,
                        'name' => $social_user->name,
                        'email'    => $social_user->email,
                        'password' => Hash::make(str_random(60)),
                        'point' => 100
                    ];

                    if (isset($social_user->nickname)) {
                        if (!is_null($social_user->nickname)) {
                            $data['provider_username'] = $social_user->nickname;
                        }
                    }

                    if (isset($social_user->profileUrl)) {
                        $pattern = '/(https?:\/\/)?(www\.)?facebook\.com\/(app_scoped_user_id*\/)*([A-Za-z0-9]+)[?]?.*/';
                        $url = $social_user->profileUrl;

                        if (preg_match($pattern, $url, $match)) {
                            $data['provider_profile_id'] = $match[4];
                        }
                    }

                    $user = User::create($data);

                    $create_point = $user->points()->create(
                        [
                            'point_id' => 1,
                            'points' => 100,
                        ]
                    );

                    if ($create_point) {
                        $user_points = $user->points;
                        $user->point = $user_points->sum('points');
                        $user->save();
                    }

                    HelperImage::upload_user_image($social_user->avatar_original, $user);
                }

                auth()->login($user, true);

                return redirect()->route('home');
            }
        }

        abort(404);
    }

    public function optOut()
    {

    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('home');
    }

    private function validProvider($provider)
    {
        if (isset($this->provider_list[$provider])) {
            $this->provider = $this->provider_list[$provider];
            return true;
        }

        abort(404);
    }
}
