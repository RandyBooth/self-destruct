<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlanResource;
use App\Traits\PointTrait;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    use PointTrait;

    public function __construct()
	{
		$this->middleware(
			['auth']
		);
	}

	public function index()
	{
		$user = auth()->user();
		$cards = [];
		$state = null;

        $plans = $this->getPlanPoints('slug');

        if (!empty($plans)) {
            if (request()->has('plan')) {
                $plan = request()->get('plan');

                if (isset($plans[$plan])) {
                    $plans[$plan]['default'] = true;
                }
            }

            $state['plans'] = PlanResource::collection($plans->values());
        }

        if (!empty($user->stripe_id)) {
			$cards_data = $user->cards;

			foreach($cards_data as $card) {
				$card_id = cache()->remember('card:'.$card['id'], 5, function () use ($card) {
                    return encrypt($card['id']);
				});

				$cards[] = [
					'id' => $card_id,
                    'default' => $card['default'],
                    'brand' => $card['brand'],
                    'last_four' => $card['last_four'],
                    'expiry_date' => $card['expiry_date'],
				];
			}

			if (!empty($cards)) {
				$state['cards'] = $cards;
			}
		}

		if (!empty($state)) {
            $state = collect($state);
        }

		return view('checkouts.index', compact('state'));
	}

//	public function invoices()
//	{
//		try {
//			$user = User::find(1);
//
//			$invoices = $user->invoices();
//
//			return view('invoices', compact('invoices'));
//
//		} catch (\Exception $ex) {
//			return $ex->getMessage();
//		}
//
//	}
//
//	public function invoice($invoice_id)
//	{
//		try {
//			$user = User::find(1);
//
//			return $user->downloadInvoice($invoice_id, [
//				'vendor'  => 'Your Company',
//				'product' => 'Your Product',
//			]);
//
//		} catch (\Exception $ex) {
//			return $ex->getMessage();
//		}
//	}
}
