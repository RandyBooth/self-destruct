<?php

namespace App\Http\Controllers;

use App\UserOldVote;
use Illuminate\Http\Request;

class UserOldVoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserOldVote  $UserOldVote
     * @return \Illuminate\Http\Response
     */
    public function show(UserOldVote $UserOldVote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserOldVote  $UserOldVote
     * @return \Illuminate\Http\Response
     */
    public function edit(UserOldVote $UserOldVote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserOldVote  $UserOldVote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserOldVote $UserOldVote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserOldVote  $UserOldVote
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserOldVote $UserOldVote)
    {
        //
    }
}
