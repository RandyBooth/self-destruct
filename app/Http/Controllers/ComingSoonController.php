<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Carbon\Carbon;

class ComingSoonController extends Controller
{
    public function index()
    {
        $now = Carbon::now();
        $now_timestamp = $now->timestamp;

        $end = Carbon::parse('2018-10-15 12:00:00');
        $end_timestamp = $end->timestamp;
        $date = $end->format('F d, Y h:i A');

        $diff = $end_timestamp - $now_timestamp;

        if ($diff < 0) {
            $diff = 0;
        }

        return view('coming-soon', compact('diff', 'date'));
    }
}
