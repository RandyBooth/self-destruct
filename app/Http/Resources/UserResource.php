<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $provider_slug = strtolower($this->provider);
        $provider_icon = $provider_slug;
        $voted = false;

        switch ($provider_slug) {
            case 'facebook':
                $provider_icon = $provider_slug.'-f';
                break;
        }

        if (isset($this->voted)) {
            $voted = $this->voted;
        }

        return [
            'id' => $this->encoded_id,
            'name' => $this->name,
            'image' => $this->image,
            'provider' => $this->provider,
            'provider_id' => $this->provider_id,
            'provider_slug' => $provider_slug,
            'provider_icon' => $provider_icon,
            'vote' => $this->vote,
            'voted' => $voted,
            'destructed' => !is_null($this->destructed_at),
            'friends' => UserResource::collection($this->whenLoaded('friends')),
        ];
    }
}
