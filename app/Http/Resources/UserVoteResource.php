<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserVoteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->encoded_id,
            'voted' => $this->when($this->voted, true, false)
        ];
    }
}
