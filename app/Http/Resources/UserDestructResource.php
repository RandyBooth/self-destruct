<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserDestructResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $provider_slug = strtolower($this->provider);
        $provider_icon = $provider_slug;

        switch ($provider_slug) {
            case 'facebook':
                $provider_icon = $provider_slug.'-f';
                break;
        }

        return [
            'id' => $this->encoded_id,
            'name' => $this->name,
            'image' => $this->image,
            'provider' => $this->provider,
            'provider_id' => $this->provider_id,
            'provider_slug' => $provider_slug,
            'provider_icon' => $provider_icon,
            'date_diff' => $this->destructed_at->diffForHumans(),
        ];
    }
}
