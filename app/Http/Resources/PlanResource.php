<?php

namespace App\Http\Resources;

use App\Traits\PointTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    use PointTrait;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $friend_point = $this->getFriendPoint();
        $friends = null;

        if ($friend_point) {
            $points =  abs($friend_point->points);
            $friends = $this->points / $points;
        }

        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'amount' => '$'.$this->amount,
            'points' => $this->points.' '.str_plural('point', $this->points),
            'friends' => $this->when($friends, $friends.' '.str_plural('friend', $friends)),
            'default' => $this->when($this->default, true),
        ];
    }
}
