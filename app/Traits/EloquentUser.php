<?php

namespace App\Traits;

use App\Http\Resources\UserResource;
use App\Http\Resources\UserDestructResource;
use App\User;
use App\UserFriend;
use App\UserVote;
use Carbon\Carbon;
use Cog\Laravel\Optimus\Facades\Optimus;

trait EloquentUser
{
	protected function apiUsers($path = null)
	{
		$users = $this->getUsers();

		return (empty($path))
			? UserResource::collection($users)
			: collect([ 'path' => $path, 'users' => UserResource::collection($users), 'time' => $this->getTimeDiff(2), 'start' => $this->isStart()]);
	}

    protected function apiUser($provider, $provider_id, $path = null)
    {
        $user = $this->findProvider($provider, $provider_id, true);

        $this->getUserVoted($user);

        return (empty($path))
            ? [new UserResource($user)]
            : collect([ 'path' => $path, 'users' => [new UserResource($user)]]);
    }

	protected function apiUsersDestruct($path = null)
	{
		$users = $this->getUsersDestruct();

		return (empty($path))
			? UserDestructResource::collection($users)
			: collect([ 'path' => $path, 'users' => UserDestructResource::collection($users)]);
	}

    protected function getUserVoted(&$users)
    {
        if (!empty($users)) {
            $is_single = false;

            if ($users instanceof User) {
                $users_id = [$users->id];
                $is_single = true;
            } else {
                $users_id = array_pluck($users, 'id');
            }

            if (!empty($users_id)) {
                if (!empty($users_id[0])) {
	                $guard = null;

	                if (auth('api')->check()) {
		                $guard = 'api';
	                } else if (auth('web')->check()) {
		                $guard = 'web';
	                }

	                if (!empty($guard)) {
                        $id = auth($guard)->id();

                        $cache_name = $is_single ? $id.'_'.$users->id : $id;

                        $user_voted = cache()->remember('user_votes:'.$cache_name, 5, function () use ($id, $users_id) {
                            return UserVote::where('user_id', $id)->whereIn('vote_id', $users_id)->get(['user_id', 'vote_id']);
                        });

                        if (!$user_voted->isEmpty()) {
                            $user_voted_array = array_pluck($user_voted, 'user_id', 'vote_id');

                            if ($is_single) {
                                $users->voted = (isset($user_voted_array[$users->id])) ? true : false;
                            } else {
                                foreach ($users as $user) {
                                    $user->voted = (isset($user_voted_array[$user->id])) ? true : false;
                                }
                            }
                        }

                        return $users;
                    }
                }
            }
        }

        return [];
    }

	protected function getUsers($limit = null, $minutes = 5)
	{
	    $name = '';

	    if (is_int($limit)) {
            $name = '-'.$limit;
        } else {
            $limit = 10;
        }

        if ($this->isStart(5)) {
            $users = cache()->remember('users'.$name, $minutes, function () use ($limit) {
                return User
                    ::ignoreDestructed()
                    ->ignoreFlagged()
                    ->orderByVote()
                    ->limit($limit)
                    ->get(['id', 'provider', 'provider_id', 'name', 'vote', 'image', 'created_at']);
            });
        } else {
            cache()->forget('users_refresh');
            $users = cache()->remember('users_refresh'.$name, $minutes, function () use ($limit) {
                $users = User
                    ::ignoreDestructed()
                    ->ignoreFlagged()
                    ->inRandomOrder()
                    ->limit($limit)
                    ->get(['id', 'provider', 'provider_id', 'name', 'vote', 'image', 'created_at']);

                $sorted = $users->sort(function($a, $b) {
                    return $b['vote'] <=> $a['vote'] ?: $b['created_at'] <=> $a['created_at'];
                });

                return $sorted->values();
            });
        }

        foreach($users as $user) {
            $friends = cache()->remember('user_friends:'.$user->encoded_id, 60, function () use ($user) {
                return $user->friends()
                    ->select(['users.id', 'provider', 'provider_id', 'name', 'image'])
                    ->orderByDesc('pivot_created_at')
                    ->limit(5)
                    ->get();
            });

            $user->setRelation('friends', $friends);
        }

	    $this->checkVotes($users);

		return $users;
	}

	protected function isStart($seconds = 0)
    {
        $now = Carbon::now();
        $now->addSeconds($seconds);
        $now_format = $now->format('YmdHis');

        $start = env('APP_START_2');

        return ($now_format >= $start);
    }

    protected function checkVotes(&$users)
    {
        $is_vote_update = false;

        foreach($users as &$user) {
            if ($this->checkVote($user) && !$is_vote_update) {
                $is_vote_update = true;
            }
        }

        if ($is_vote_update) {
            $sorted = $users->sort(function($a, $b) {
                return $b['vote'] <=> $a['vote'] ?: $b['created_at'] <=> $a['created_at'];
            });

            $users = $sorted->values();
        }

        return;
    }

    protected function checkVote(&$user)
    {
        if (cache()->has('user_vote:'.$user->encoded_id)) {
            $user_vote = cache()->get('user_vote:'.$user->encoded_id);

            if ($user_vote > $user->vote) {
                $user->vote = (int) $user_vote;

                return true;
            }
        }

        return false;
    }

	protected function getUsersDestruct()
	{
		$users = cache()->remember('users_destruct', 5, function () {
			return User
				::whereNotNull('destructed_at')
				->ignoreFlagged()
                ->orderBy('destructed_at')
                ->limit(10)
				->get();
		});

		return $users;
	}

    protected function clearCacheUserEncodedId($encoded_id)
    {
        cache()->forget('user_encodedid:'.$encoded_id);
    }

    protected function clearCacheUserVotes($id, $extra_id = null)
    {
        $cache_name = (!empty($extra_id)) ? $id.'_'.$extra_id: $id;

        cache()->forget('user_votes:'.$cache_name);
    }

    protected function clearCacheUserVote($encoded_id)
    {
        cache()->forget('user_vote:'.$encoded_id);
    }

    protected function clearCacheUserSocial($provider, $provider_id)
    {
        $provider = strtolower($provider);
        cache()->forget('social_provider:'.$provider.'_'.$provider_id);
        cache()->forget('social_provider_fr:'.$provider.'_'.$provider_id);
    }

    protected function refreshUsers()
    {
        cache()->forget('users_refresh');
        cache()->forget('end_timestamp');
        event(new \App\Events\UserList());
    }

    protected function destroyTopUser()
    {
        $user = User
            ::ignoreDestructed()
            ->ignoreFlagged()
            ->orderByVote()
            ->firstOrFail();

        $encoded_id = $user->encoded_id;
        $provider = $user->provider;
        $provider_id = $user->provider_id;

        $votes = $user->votes->toArray();
        $votes_id = [];
        $votes_data = [];

        foreach ($votes as $vote) {
            $votes_id[] = $vote['id'];
            $votes_data[] = [
                'user_id' => $vote['user_id'],
                'vote_id' => $vote['vote_id'],
                'ip_address' => $vote['ip_address'],
                'old_created_at' => $vote['created_at'],
            ];
        }

        \App\UserVote::destroy($votes_id);

        \App\UserOldVote::insert($votes_data);

        $user->update(['vote' => 0, 'destructed_at' => now()]);

        $this->clearCacheUserEncodedId($encoded_id);
        $this->clearCacheUserVote($encoded_id);
        $this->clearCacheUserSocial($provider, $provider_id);

        $this->refresh(true, true);
    }

    protected function findUserDecode($encoded_id)
    {
        return cache()->remember('user_encodedid:'.$encoded_id, 5, function () use ($encoded_id) {
            return User::findOrFail(Optimus::connection('user')->decode($encoded_id));
        });
    }

    protected function findProvider($provider, $provider_id, $show_friends = false)
    {
        $provider = strtolower($provider);

        $user = cache()->remember('social_provider:'.$provider.'_'.$provider_id, 5, function () use ($provider, $provider_id) {
            return User
                ::where('provider', $provider)
                ->where('provider_id', $provider_id)
                ->firstOrFail(['id', 'provider', 'provider_id', 'provider_username', 'provider_profile_id', 'name', 'vote', 'image', 'destructed_at', 'created_at']);
        });

        if ($show_friends) {
            $friends = cache()->remember('social_provider_fr:'.$provider.'_'.$provider_id, 60, function () use ($user, $provider, $provider_id) {
                return $user->friends()
                    ->select(['users.id', 'provider', 'provider_id', 'name', 'image'])
                    ->orderByDesc('pivot_created_at')
                    ->get();
            });

            $user->setRelation('friends', $friends);
        }

        $this->checkVote($user);

        return $user;
    }

    protected function cacheUserVote($encoded_id, $vote)
    {
        $name = 'user_vote:'.$encoded_id;

        if (cache()->has($name)) {
            cache()->increment($name);
        } else {
            cache()->put($name, $vote, 5);
        }
    }

    protected function check($user)
    {
        $destroy = false;
        $users = $this->getUsers();
        $new_users = clone $users;

        $users = $users->map(function ($item) {
            return collect($item)->only(['encoded_id', 'vote', 'created_at']);
        });

        $users = $users->keyBy('encoded_id');

        $new_users = $new_users->keyBy('encoded_id');

        $new_users->put($user->encoded_id, $user);

        $new_users = $new_users->map(function ($item) {
            return collect($item)->only(['encoded_id', 'vote', 'created_at']);
        });

        $sorted_new_users = $new_users->sort(function($a, $b) {
            return $b['vote'] <=> $a['vote'] ?: $b['created_at'] <=> $a['created_at'];
        });

        $new_users = $sorted_new_users->values();

        $users_keys = $users->pluck('encoded_id');
        $new_users_keys = $new_users->pluck('encoded_id');

        $count_users = $users->count();
        $count_new_users = $new_users->count();

        if ($count_users !== $count_new_users) {
            $new_users_chunk = $new_users_keys->slice(0, $count_users);

            if ($users_keys != $new_users_chunk) {
                $destroy = true;
                $this->refresh($destroy);
            }
        } else if ($users_keys != $new_users_keys) {
            $destroy = true;
            $this->refresh($destroy);
        }

        return $destroy;
    }

	protected function refresh($destroy = false, $force_refresh = false)
	{
		if ($destroy) {
			cache()->forget('users');

			$destroy_users = $this->getUsersDestruct();

			if ($destroy_users->count() < 10) {
                cache()->forget('users_destruct');
                event(new \App\Events\UserDestruct());
            }
		}

		event(new \App\Events\UserList($force_refresh));
	}

	protected function refreshDestruct()
	{
        $users = $this->getUsers();

        if ($users->count() < 10) {
            cache()->forget('users');
            event(new \App\Events\UserList(true));
        }

		cache()->forget('users_destruct');
		event(new \App\Events\UserDestruct());
	}

	protected function saveFriend($user_id, $friend_id)
    {
        $user = new UserFriend;
        $user->user_id = $user_id;
        $user->friend_id = $friend_id;

        return $user->save();
    }

    private function getTimeDiff($sub_second = 0)
    {
        $now = Carbon::now();
        $now_timestamp = $now->timestamp;
        $now_format = $now->format('YmdHis');

        $start = env('APP_START_2');

        if ($now_format >= $start) {
//            if ($now->minute >= 0 && $now->minute < 30) { // 0..29, 30..59
//                $minute = 29;
//            } else {
                $minute = 59;
//            }

            $second = 59;

            if (!cache()->has('end_of_hour_timestamp')) {
                $end_of_hour = $now->copy();
                $end_of_hour->minute = $minute;
                $end_of_hour->second = $second;
                $end_of_hour_timestamp = $end_of_hour->timestamp;
                cache()->put('end_of_hour_timestamp', $end_of_hour_timestamp, 60*2);
            } else {
                $end_of_hour_timestamp = cache()->get('end_of_hour_timestamp');
            }

            $diff = $end_of_hour_timestamp - $now_timestamp;

            if ($diff <= 0) {
                if (!isset($end_of_hour)) {
                    $end_of_hour = $now->copy();
                    $end_of_hour->minute = $minute;
                    $end_of_hour->second = $second;
                }

                $end_of_hour_timestamp = $end_of_hour->timestamp;
                $diff = $end_of_hour_timestamp - $now_timestamp;
                cache()->put('end_of_hour_timestamp', $end_of_hour_timestamp, 60*2);
            }
        } else {
            if (!cache()->has('end_timestamp')) {
                $end = Carbon::parse($start);
                $end_timestamp = $end->timestamp;
                cache()->put('end_timestamp', $end_timestamp, 60*2);
            } else {
                $end_timestamp = cache()->get('end_timestamp');
            }

            $diff = $end_timestamp - $now_timestamp;
        }

        if ($sub_second > 0) {
            $sub_diff = $diff - $sub_second;

            $diff = ($sub_diff > 0) ? $sub_diff : 0;
        }

        return $diff;
    }
}
