<?php

namespace App\Traits;

use App\Point;

trait PointTrait
{
	protected function getPlanPoints($keyBy = null)
	{
        $plan_points = cache()->remember('plan-points', 60, function () {
            return Point
                ::where('points', '>', 0)
                ->orderBy('points')
                ->get([
                    'id',
                    'name',
                    'slug',
                    'amount',
                    'points',
                ]);
        });

        return (!empty($keyBy)) ? $plan_points->keyBy($keyBy) : $plan_points;
    }

	protected function clearPlanPoints()
	{
		cache()->forget('plan-points');
	}

	protected function getFriendPoint()
	{
		return cache()->remember('friend-point', 60, function () {
			return Point::where('slug', 'friend')->first([
				'id',
				'name',
				'slug',
				'amount',
				'points',
			]);
		});
	}

	protected function clearFriendPoint()
	{
		cache()->forget('friend-point');
	}
}
