<?php

namespace App\Traits;

trait ProvidesModelCacheKey
{
    protected function cacheKey()
    {
        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $this->updated_at->timestamp
        );
    }
}
