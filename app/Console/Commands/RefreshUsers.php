<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\EloquentUser;

class RefreshUsers extends Command
{
    use EloquentUser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger()->info('Refresh users');
        $this->refreshUsers();
    }
}
