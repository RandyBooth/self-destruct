<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\RefreshUsers',
        '\App\Console\Commands\RemoveTopUser',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $now = Carbon::now();
        $now->subSeconds(5);
        $now_format = $now->format('YmdHis');

        $start = env('APP_START_2');

        if ($now_format > $start) {
            $schedule->command('user:top-destroy')
                ->hourly()
//                ->everyThirtyMinutes()
                ->evenInMaintenanceMode();
        } else {
            $schedule->command('users:refresh')
                 ->everyFiveMinutes()
                 ->evenInMaintenanceMode();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
