<?php

namespace App;

use Cog\Laravel\Optimus\Traits\OptimusEncodedRouteKey;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,
        Notifiable,
        OptimusEncodedRouteKey,
        SoftDeletes;

    protected $appends = ['encoded_id'];
    protected $dates = ['deleted_at', 'destructed_at', 'flagged_at', 'image_updated_at', 'renewed_at'];
    protected $fillable = ['provider', 'provider_id', 'provider_username', 'provider_profile_id', 'name', 'email', 'password', 'vote', 'point', 'image', 'stripe_id', 'destructed_at', 'flagged_at', 'image_updated_at', 'renewed_at'];
    protected $hidden = ['id', 'password', 'remember_token'];
    protected $optimusConnection = 'user';

    public function getEncodedIdAttribute()
    {
        return $this->getRouteKey();
    }

    public function getLinkAttribute()
    {
        return route('users.show', [$this->slug()]);
    }

    public function getProviderLinkAttribute()
    {
        return route('users.provider_link', [$this->slug()]);
    }

    public function scopeIgnoreDestructed($query)
    {
        return $query->where('destructed_at', null);
    }

    public function scopeIgnoreFlagged($query)
    {
        return $query->where('flagged_at', null);
    }

    public function scopeOrderByVote($query)
    {
        return $query->orderByDesc('vote')->orderByDesc('created_at');
    }

    public function destructed()
    {
        return $this->update(['destructed_at' => now()]);
    }

    public function UndoDestructAndRenewed()
    {
        return $this->update(['destructed_at' => null, 'renewed_at' => now()]);
    }

    public function UndoDestruct()
    {
        return $this->update(['destructed_at' => null]);
    }

    public function flag()
    {
        return $this->update(['flagged_at' => now()]);
    }

	public function renew()
	{
		return $this->update(['renewed_at' => now()]);
	}

	public function cards()
	{
		return $this->hasMany(Card::class);
	}

    public function friends()
    {
        return $this->belongsToMany(User::class, 'user_friends', 'friend_id', 'user_id')->withTimestamps();
    }

    public function friends5()
    {
        return $this->belongsToMany(User::class, 'user_friends', 'friend_id', 'user_id')->withTimestamps();
    }

	public function points()
	{
		return $this->hasMany(UserPoint::class)->orderByDesc('created_at');
	}

	public function votes()
	{
		return $this->hasMany(UserVote::class, 'vote_id');
	}
}
