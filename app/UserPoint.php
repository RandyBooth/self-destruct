<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPoint extends Model
{
	public $timestamps = false;

	protected $fillable = ['user_id', 'point_id', 'points', 'stripe_charge_id'];
}
