<?php
namespace App\Helpers;

use File;
use Image;
use Webpatser\Uuid\Uuid;

class HelperImage
{
    public static function upload_user_image($image = null, $user = null)
    {
        if (!empty($image) && !empty($user->id)) {
            $filename_tmp = 'tmp--'.$user->id.'.jpg';
            $path = storage_path('app/public/images_upload/');
            $path_tmp = storage_path('app/public/images_tmp/');

            $new_image = Image::make($image)->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->interlace()->save($path_tmp.$filename_tmp);

            if (!empty($new_image->filesize())) {
                $str = '#'.$user->id.'/'.$new_image->filesize();

                $random = Uuid::generate(5, $str, '48cd33d1-21c0-4fd9-9b5b-b892539b0a66')->string; // name-based using SHA-1 hashing, UUID

                if (!empty($random)) {
                    $old_image = $user->image;

                    if (!Uuid::compare($random, str_replace_last('.jpg', '', $old_image))) {
                        $filename = $random.'.jpg';

                        if (File::move($path_tmp.$filename_tmp, $path.$filename)) {
                            $new_image->destroy();

                            if (File::exists($path.$filename)) {
                                $user->update(['image' => $filename, 'image_updated_at' => now()]);

                                if (!empty($old_image)) {
                                    if (File::exists($path.$old_image)) {
                                        $trash_image = storage_path('app/public/images_trash/'.$old_image);

                                        if (File::move($path.$old_image, $trash_image)) {
                                            touch($trash_image);
                                        }
                                    }
                                }
                            }

                            return $filename;
                        }
                    }
                }

                if (File::exists($path_tmp.$filename_tmp)) {
                    File::delete($path_tmp.$filename_tmp);
                }
            }

            $user->update(['image_updated_at' => now()]);
        }

        return false;
    }
}
