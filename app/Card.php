<?php

namespace App;

use Cog\Laravel\Optimus\Traits\OptimusEncodedRouteKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
	use OptimusEncodedRouteKey,
        SoftDeletes;

    protected $appends = ['encoded_id'];
    protected $fillable = ['user_id', 'stripe_id', 'brand', 'last_four', 'expiry_date', 'default'];
    protected $hidden = ['id'];
    protected $optimusConnection = 'card';

    public function getEncodedIdAttribute()
    {
        return $this->getRouteKey();
    }
}
