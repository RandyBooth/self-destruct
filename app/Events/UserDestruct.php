<?php

namespace App\Events;

use App\Traits\EloquentUser;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
//use Illuminate\Broadcasting\PrivateChannel;
//use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserDestruct implements ShouldBroadcast
{
    use Dispatchable, EloquentUser, InteractsWithSockets, SerializesModels;

	public $users;
	public $refresh;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
	    $data = $this->apiUsersDestruct('/destruct');

	    $this->users = $data['users'];
	    $this->refresh = true;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('users-destruct');
    }
}
