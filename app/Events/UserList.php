<?php

namespace App\Events;

use App\Traits\EloquentUser;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserList implements ShouldBroadcast
{
    use Dispatchable, EloquentUser, InteractsWithSockets, SerializesModels;

    public $users;
    public $time;
    public $refresh;
    public $start;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($refresh = false)
    {
        $data = $this->apiUsers('/');

        if ($data['time'] > 0) {
            $this->users = $data['users'];
            $this->time = $data['time'];
            $this->refresh = $refresh;
            $this->start = $this->isStart();
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('users');
    }
}
