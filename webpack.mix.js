let mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/coming-soon.js', 'public/js')
    .js('resources/assets/js/bootstrap.js', 'public/js')
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/stripe.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .purgeCss({
        whitelistPatterns: [/^fa-facebook-f/, /^fa-twitter/, /^color-/, /^flash__/, /^error$/, /^success$/],
    });

if (mix.inProduction()) {
    mix.version();
}